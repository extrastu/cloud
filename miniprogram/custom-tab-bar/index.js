Component({
  options: {
    addGlobalClass: true,
  },
  data: {
    selected: 0,
    list: [
      {
        "pagePath": "/pages/index/index",
        "iconPath": "/public/icon/home-unselect.png",
        "selectedIconPath": "/public/icon/home-selected.png",
        "text": ""
      },
      {
        "pagePath": "/pages/search/index",
        "iconPath": "/public/icon/search-unselect.png",
        "selectedIconPath": "/public/icon/search-selected.png",
        "text": ""
      }
    ]
  },
  methods: {
    switchTab(e) {      
      const url = e.currentTarget.dataset.path
      wx.switchTab({
        url
      })
    }
  },
  pageLifetimes: {
  },
})