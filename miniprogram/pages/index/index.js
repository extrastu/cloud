//index.js
//获取应用实例
const app = getApp()
let util = require('../../utils/util');
Page({
  data: {
    Custom: app.globalData.Custom,
    StatusBar: app.globalData.StatusBar,
    CustomBar: app.globalData.CustomBar,
    cardCur: 0,
    tower: [{
      id: 0,
      url: 'https://image.weilanwl.com/img/4x3-1.jpg'
    }, {
      id: 1,
      url: 'https://image.weilanwl.com/img/4x3-2.jpg'
    }, {
      id: 2,
      url: 'https://image.weilanwl.com/img/4x3-3.jpg'
    }, {
      id: 3,
      url: 'https://image.weilanwl.com/img/4x3-4.jpg'
    }, {
      id: 4,
      url: 'https://image.weilanwl.com/img/4x3-2.jpg'
    }, {
      id: 5,
      url: 'https://image.weilanwl.com/img/4x3-4.jpg'
    }, {
      id: 6,
      url: 'https://image.weilanwl.com/img/4x3-2.jpg'
    }],
    iconList: [{
      icon: 'github',
      color: 'red',
      badge: 120,
      name: 'Github Daily'
    }, {
      icon: 'apps',
      color: 'red',
      badge: 0,
      name: 'App'
    }, {
      icon: 'picfill',
      color: 'red',
      badge: 0,
      name: '壁纸'
    }, {
      icon: 'noticefill',
      color: 'red',
      badge: 22,
      name: '通知'
    }],
    skin: false,
    oneList:[],
    loading:true
  },
  onLoad() {
    this.towerSwiper('tower');
    // this.getOneData();
    // this.getGuanzhi();
    console.log(util.formatTimeTwo('1550582024', 'Y/M/D h:m:s'))
    // 初始化towerSwiper 传已有的数组名即可
    this.getDate();
  },
  DotStyle(e) {
    this.setData({
      DotStyle: e.detail.value
    })
  },
  // cardSwiper
  cardSwiper(e) {
    this.setData({
      cardCur: e.detail.current
    })
  },
  // towerSwiper
  // 初始化towerSwiper
  towerSwiper(name) {
    let list = this.data[name];
    for (let i = 0; i < list.length; i++) {
      list[i].zIndex = parseInt(list.length / 2) + 1 - Math.abs(i - parseInt(list.length / 2))
      list[i].mLeft = i - parseInt(list.length / 2)
    }
    this.setData({
      towerList: list
    })
  },

  // towerSwiper触摸开始
  towerStart(e) {
    this.setData({
      towerStart: e.touches[0].pageX
    })
  },

  // towerSwiper计算方向
  towerMove(e) {
    this.setData({
      direction: e.touches[0].pageX - this.data.towerStart > 0 ? 'right' : 'left'
    })
  },

  // towerSwiper计算滚动
  towerEnd(e) {
    let direction = this.data.direction;
    let list = this.data.towerList;
    if (direction == 'right') {
      let mLeft = list[0].mLeft;
      let zIndex = list[0].zIndex;
      for (let i = 1; i < list.length; i++) {
        list[i - 1].mLeft = list[i].mLeft
        list[i - 1].zIndex = list[i].zIndex
      }
      list[list.length - 1].mLeft = mLeft;
      list[list.length - 1].zIndex = zIndex;
      this.setData({
        towerList: list
      })
    } else {
      let mLeft = list[list.length - 1].mLeft;
      let zIndex = list[list.length - 1].zIndex;
      for (let i = list.length - 1; i > 0; i--) {
        list[i].mLeft = list[i - 1].mLeft
        list[i].zIndex = list[i - 1].zIndex
      }
      list[0].mLeft = mLeft;
      list[0].zIndex = zIndex;
      this.setData({
        towerList: list
      })
    }
  },
  isCard(e) {
    this.setData({
      isCard: e.detail.value
    })
  },
  getOneData(){
    let that = this;
    wx.request({
      url: 'http://localhost:3000/one', //仅为示例，并非真实的接口地址
      success: function (res) {
        that.setData({
          oneList: res.data
        })
      }
    })
  },
  getGuanzhi(){
    let that = this;
    wx.request({
      url: 'http://localhost:3000/guanzhi', //仅为示例，并非真实的接口地址
      success: function (res) {
        that.setData({
          oneList: that.data.oneList.concat(res.data) 
        })
      }
    })
  },
  enterPost(){
    wx.navigateTo({
      url: '/pages/post/index'
    })
  },
  getDate(){
    Promise.all([this.getOneData(),this.getGuanzhi()]).then(
      function (data) {
      }
    ).catch(function (err) {
      getAllDataAndSendMail() //再次获取
      console.log('获取数据失败： ', err);
    })
  },
  intoItem(e){
    var type = e.currentTarget.dataset.type;
    wx.navigateTo({
      url: '/pages/'+type+'/index'
    })
  }
})